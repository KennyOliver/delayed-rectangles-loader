# delayed-rectangles-loader

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/delayed-rectangles-loader/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/delayed-rectangles-loader?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/delayed-rectangles-loader?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/delayed-rectangles-loader?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/delayed-rectangles-loader)](https://replit.com/@KennyOliver/delayed-rectangles-loader)

**Delayed rectangles in a CSS loader**

[![GitHub Pages](https://img.shields.io/badge/See%20Demo-252525?style=for-the-badge&logo=safari&logoColor=white&link=https://kennyoliver.github.io/delayed-rectangles-loader)](https://kennyoliver.github.io/delayed-rectangles-loader)

---
Kenny Oliver ©2021
